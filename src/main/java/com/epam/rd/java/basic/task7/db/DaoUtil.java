package com.epam.rd.java.basic.task7.db;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public final class DaoUtil {

    private static Properties prop;

    static {
        prop = new Properties();
        try {
            prop.load(new FileInputStream("app.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private DaoUtil() {
        //private constructor
    }


    public static Statement getStatement(){
        try {
            Connection con = getConnection();
            return con.createStatement();
        }catch (SQLException e){
            e.printStackTrace();
            throw new RuntimeException();
        }
    }

    public static Statement getPreparedStatement(String query){
        try {
            Connection con = getConnection();
            return con.prepareStatement(query);
        }catch (SQLException e){
            e.printStackTrace();
            throw new RuntimeException();
        }
    }

    public static Connection getConnection() throws SQLException {
        String property = prop.getProperty("connection.url");
        return DriverManager.getConnection(property);
    }
}
