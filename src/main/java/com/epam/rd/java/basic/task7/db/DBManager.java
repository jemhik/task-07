package com.epam.rd.java.basic.task7.db;

import com.epam.rd.java.basic.task7.db.entity.Team;
import com.epam.rd.java.basic.task7.db.entity.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static com.epam.rd.java.basic.task7.db.DaoUtil.getConnection;


public class DBManager {

	private static DBManager instance;


	public static synchronized DBManager getInstance() {
		if (instance == null) {
			instance = new DBManager();
		}
		return instance;
	}

	private DBManager() {
	}

	public List<User> findAllUsers() throws DBException {

		try (Connection con = getConnection();
			 Statement stmt = con.createStatement();
			 ResultSet rs = stmt.executeQuery("SELECT * FROM users")) {
			List<User> allUsers = new ArrayList<>();
			while (rs.next()) {
				allUsers.add(new User(rs.getInt("id"), rs.getString("login")));
			}
			return allUsers;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException(e.getMessage(), e);
		}
	}


	public boolean insertUser(User user) throws DBException {
		try (Connection con = getConnection();
			 PreparedStatement stmt = con.prepareStatement("INSERT INTO users(login) VALUES (?)")) {
			stmt.setString(1, user.getLogin());
			boolean success = stmt.executeUpdate() == 1;
			if(success){
				User userFromDB = getUser(user.getLogin());
				user.setId(userFromDB.getId());
			}
			return success;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException(e.getMessage(), e);
		}
	}

	public boolean deleteUsers(User... users) throws DBException {
		try (Connection con = getConnection();
			 Statement stmt = con.createStatement()) {
			StringBuilder query = new StringBuilder("DELETE FROM users WHERE id IN (");
			for (int i = 0; i < users.length - 1; i++) {
				query.append(users[i].getId()).append(",");
			}
			query.append(users[users.length - 1].getId()).append(")");
			return stmt.executeUpdate(query.toString()) != 0;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException(e.getMessage(), e);
		}
	}

	public User getUser(String login) throws DBException {
		try (Connection con = getConnection();
			 PreparedStatement stmt = con.prepareStatement("SELECT * FROM users WHERE login = ?")) {
			stmt.setString(1, login);
			ResultSet rs = stmt.executeQuery();
			rs.next();
			return new User(rs.getInt("id"), rs.getString("login"));
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException(e.getMessage(), e);
		}
	}

	public Team getTeam(String name) {
		try (Connection con = getConnection();
			 PreparedStatement stmt = con.prepareStatement("SELECT * FROM teams WHERE name = ?")) {
			stmt.setString(1, name);
			ResultSet rs = stmt.executeQuery();
			rs.next();
			return new Team(rs.getInt("id"), rs.getString("name"));
		} catch (SQLException e) {
			e.printStackTrace();
			throw new EntityNotFoundException("Team not found by name: " + name);
		}
	}

	public List<Team> findAllTeams() throws DBException {
		try (Connection con = getConnection();
			 Statement stmt = con.createStatement();
			 ResultSet rs = stmt.executeQuery("SELECT * FROM teams")) {
			List<Team> allTeams = new ArrayList<>();
			while (rs.next()) {
				allTeams.add(new Team(rs.getInt("id"), rs.getString("name")));
			}
			return allTeams;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException(e.getMessage(), e);
		}
	}

	public boolean insertTeam(Team team) throws DBException {
		try (Connection con = getConnection();
			 PreparedStatement stmt = con.prepareStatement("INSERT INTO teams(name) VALUES (?)")) {
			stmt.setString(1, team.getName());
			boolean success = stmt.executeUpdate() == 1;
			if(success){
				Team teamFromDB = getTeam(team.getName());
				team.setId(teamFromDB.getId());
			}
			return success;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException(e.getMessage(), e);
		}
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		Connection con = null;
		Statement stmt = null;
		try {
			con = getConnection();
			con.setAutoCommit(false);
			stmt = con.createStatement();
			StringBuilder query = new StringBuilder("INSERT INTO users_teams VALUES (");
			for (int i = 0; i < teams.length - 1; i++) {
				query.append(user.getId()).append(",").append(teams[i].getId()).append("),(");
			}
			query.append(user.getId()).append(",").append(teams[teams.length - 1].getId()).append(")");
			stmt.executeUpdate(query.toString());
			con.commit();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			try {
				con.rollback();
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
			throw new DBException(e.getMessage(), e);
		} finally {
			try {
				stmt.close();
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}


	public List<Team> getUserTeams(User user) throws DBException {
		try (Connection con = getConnection();
			 Statement stmt = con.createStatement()) {
			ResultSet rs = stmt.executeQuery("SELECT * FROM users_teams WHERE user_id = " + user.getId());
			List<Integer> teamsIds = new ArrayList<>();
			while (rs.next()) {
				teamsIds.add(rs.getInt("team_id"));
			}
			rs.close();
			return getTeamsByIds(stmt, teamsIds);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException(e.getMessage(), e);
		}
	}

	private List<Team> getTeamsByIds(Statement stmt, List<Integer> idList) throws DBException {
		List<Team> allTeams = new ArrayList<>();
		if(idList.isEmpty()){
			return allTeams;
		}
		try{
			StringBuilder query = new StringBuilder("SELECT * FROM teams WHERE id IN(");
			for (int i = 0; i < idList.size() - 1; i++) {
				query.append(idList.get(i))
						.append(",");
			}
			query.append(idList.get(idList.size() - 1))
					.append(")");
			ResultSet rs = stmt.executeQuery(query.toString());

			while (rs.next()) {
				allTeams.add(new Team(rs.getInt("id"), rs.getString("name")));
			}
			return allTeams;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException(e.getMessage(), e);
		}
	}

	public boolean deleteTeam(Team team) throws DBException {
		try (Connection con = getConnection();
			 Statement stmt = con.createStatement()) {
			stmt.executeUpdate("DELETE FROM users_teams WHERE team_id = " + team.getId());
			return stmt.executeUpdate("DELETE FROM teams WHERE id = " + team.getId()) != 0;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException(e.getMessage(), e);
		}
	}

	public boolean updateTeam(Team team) throws DBException {
		try (Connection con = getConnection();
			 PreparedStatement stmt = con.prepareStatement("UPDATE teams SET name = ? WHERE id = ?")) {
			stmt.setString(1, team.getName());
			stmt.setInt(2, team.getId());
			return stmt.executeUpdate() != 0;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException(e.getMessage(), e);
		}
	}

}